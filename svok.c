/********************************************************************************
 * svok (executable file)                                                       *
 *                                                                              *
 *  Author: Kai Peter, ©2022 - present                                          *
 * License: ISC                                                                 *
 * Version: 0.77                                                                *
 *                                                                              *
 * Description:                   *
 *******************************************************************************/
#define ME "svok"
#define VERSION "0.77"

#include <unistd.h>
#include "errmsg.h"
#include "open.h"

int main(int argc,char **argv)
{
  int fd;

  if (!argv[1])
    errint(ESOFT,"svok: usage: svok dir");

  if (chdir(argv[1]) == -1)
    errint(EHARD,B("unable to chdir to ",argv[1],": "));

  fd = open_write("supervise/ok");
  if (fd == -1) {
    if (errno == ENOENT) _exit(100);
    if (errno == ENXIO) _exit(100);
    errint(FATAL,B("unable to open ",argv[1],"/supervise/ok: "));
  }

  _exit(0);
}
