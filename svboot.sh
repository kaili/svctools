#********************************************************************************
# svboot (script executable)                                                    *
#                                                                               *
#  Author: Kai Peter, ©2022-present                                             *
#  Origin: D.J. Bernstein (daemontools-0.76), ©1999-2001, 2007                  *
# Version: 77.2.1                                                               *
# License: ISC                                                                  *
#                                                                               *
# Description: Scan the service directory and start services.                   *
#********************************************************************************
VERSION=`cat $0 | grep -m 1 'Version' | cut -d\   -f3`       # get version number

PATH=QSVPATH

exec <DEVNULL
exec >DEVNULL
exec 2>DEVNULL

QBINDIR/svc -dx QSVCDIR/* QSVCDIR/*/log

env - PATH=$PATH svscan QSVCDIR 2>&1 | \
env - PATH=$PATH service messages: ................................................................................................................................................................................................................................................................................................................................................................................................................
