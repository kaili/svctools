#********************************************************************************
echo -e "\033[1mInstalling svctools ...\033[0m"          # start the installation

_chown() { chown -f $1 $2 || ERRCHOWN=1; }

# directories defined in conf-build
PREFIX="$DESTDIR""QPREFIX"
BINDIR="$DESTDIR""QBINDIR"
MANDIR="$DESTDIR""QPREFIX/man"

[ -d "$BINDIR" ] || mkdir -pv "$BINDIR"

cp -pv envdir        "$BINDIR"; _chown 0.0 "$BINDIR"/envdir
cp -pv envuidgid     "$BINDIR"; _chown 0.0 "$BINDIR"/envuidgid
cp -pv fghack        "$BINDIR"; _chown 0.0 "$BINDIR"/fghack
#cp -pv svclogd      "$BINDIR"; _chown 0.0 "$BINDIR"/svclogd
cp -pv pgrphack      "$BINDIR"; _chown 0.0 "$BINDIR"/pgrphack
cp -pv readproctitle "$BINDIR"; _chown 0.0 "$BINDIR"/readproctitle
cp -pv setlock       "$BINDIR"; _chown 0.0 "$BINDIR"/setlock
cp -pv setuidgid     "$BINDIR"; _chown 0.0 "$BINDIR"/setuidgid
cp -pv softlimit     "$BINDIR"; _chown 0.0 "$BINDIR"/softlimit
cp -pv svboot        "$BINDIR"; _chown 0.0 "$BINDIR"/svboot
cp -pv supervise     "$BINDIR"; _chown 0.0 "$BINDIR"/supervise
cp -pv svc           "$BINDIR"; _chown 0.0 "$BINDIR"/svc
cp -pv svcmsg        "$BINDIR"; _chown 0.0 "$BINDIR"/svcmsg
cp -pv svok          "$BINDIR"; _chown 0.0 "$BINDIR"/svok
cp -pv svscan        "$BINDIR"; _chown 0.0 "$BINDIR"/svscan
cp -pv svstat        "$BINDIR"; _chown 0.0 "$BINDIR"/svstat
cp -pv svm           "$BINDIR"; _chown 0.0 "$BINDIR"/svm
#cp -pv tai64nlocal   "$BINDIR"; _chown 0.0 "$BINDIR"/tai64nlocal

# man pages
manInstall

### delete files which are removed from package (silently) ###


# finished message
echo -e "\033[1m ... finished!\033[0m"
[ "$ERRCHOWN" = "1" ] && printf "\033[93m%s\033[0m\n" \
  "Warning: ownership of some files was not set! (root privileges required)"
