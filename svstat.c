/********************************************************************************
 * svstat (executable file)                                                     *
 *                                                                              *
 *  Author: Kai Peter, ©2022 - present                                          *
 *  Origin: D.J. Bernstein (daemontools-0.76), ©1999-2001, 2007                 *
 * License: ISC                                                                 *
 * Version: 0.77.6                                                              *
 *                                                                              *
 * Description: Get the status of a daemon which is under control of supervise. *
 *******************************************************************************/
#define ME "svstat"
#define VERSION "0.77"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "buffer.h"
#include "qstrings.h"
#include "open.h"
#include "tai.h"

#include "svcdir.h"

char bspace[1024];
buffer b;

char status[18];
char strnum[FMT_ULONG];

unsigned long pid;
unsigned char normallyup;
unsigned char want;
unsigned char paused;

void usage(void) { out("Usage: svstat service [service ...] ") ; _exit(0); }

void doit(char *dir)
{
  struct stat st;
  int r;
  int fd;
  char *x;
  struct tai when;
  struct tai now;

  buffer_init(&b,write,1,bspace,sizeof(bspace));
  buffer_puts(&b,dir);
  buffer_puts(&b,": ");

  if (chdir(dir) == -1) {
    x = errstr(errno);
//    buffer_puts(&b,"unable to chdir: ");
    buffer_puts(&b,"\033[1mno such service\033[0m: ");
    buffer_puts(&b,x);
    return;
  }

  normallyup = 0;
  if (stat("down",&st) == -1) {
    if (errno != ENOENT) {
      x = errstr(errno);
      buffer_puts(&b,"unable to stat down: ");
      buffer_puts(&b,x);
      return;
    }
    normallyup = 1;
  }

  fd = open_write("supervise/ok");
  if (fd == -1) {
    if (errno == ENXIO) {
      buffer_puts(&b,"supervise not running");
      return;
    }
    x = errstr(errno);
    buffer_puts(&b,"unable to open supervise/ok: ");
    buffer_puts(&b,x);
    return;
  }
  close(fd);
  fd = open_read("supervise/status");
  if (fd == -1) {
    x = errstr(errno);
    buffer_puts(&b,"unable to open supervise/status: ");
    buffer_puts(&b,x);
    return;
  }

  r = read(fd,status,sizeof status);
  close(fd);
  if (r < sizeof status) {
    if (r == -1)
      x = errstr(errno);
    else
      x = "bad format";

    buffer_puts(&b,"unable to read supervise/status: ");
    buffer_puts(&b,x);
    return;
  }

  pid = (unsigned char) status[15];
  pid <<= 8; pid += (unsigned char) status[14];
  pid <<= 8; pid += (unsigned char) status[13];
  pid <<= 8; pid += (unsigned char) status[12];

  paused = status[16];
  want = status[17];

  tai_unpack(status,&when);
  tai_now(&now);
  if (tai_less(&now,&when)) when = now;
  tai_sub(&when,&now,&when);

  if (pid) {
    buffer_puts(&b,"up (pid ");
    buffer_put(&b,strnum,fmt_ulong(strnum,pid));
    buffer_puts(&b,") ");
  }
  else
    buffer_puts(&b,"down ");

  unsigned int seconds = tai_approx(&when);
  unsigned int d = seconds/86400;
  seconds -= d * 86400;
  unsigned int h = seconds/3600;
  unsigned int m = (seconds - h * 3600) / 60;
  unsigned int s = seconds - h * 3600 - m * 60;

  buffer_puts(&b,fmtnum(d));
  buffer_puts(&b," day");
  if (d!=1) buffer_puts(&b,"s");
  buffer_puts(&b," ");
  buffer_puts(&b,fmtnum(h));
  buffer_puts(&b,":");
  if (m<10) buffer_puts(&b,"0");
  buffer_puts(&b,fmtnum(m));
  buffer_puts(&b,":");
  if (s<10) buffer_puts(&b,"0");
  buffer_puts(&b,fmtnum(s));
  buffer_puts(&b," hour");
  if (h!=1) buffer_puts(&b,"s");
/*
  buffer_put(&b,strnum,fmt_ulong(strnum,tai_approx(&when)));
  buffer_puts(&b," seconds");
*/
  if (pid && !normallyup)
    buffer_puts(&b,", normally down");
  if (!pid && normallyup)
    buffer_puts(&b,", normally up");
  if (pid && paused)
    buffer_puts(&b,", paused");
  if (!pid && (want == 'u'))
    buffer_puts(&b,", want up");
  if (pid && (want == 'd'))
    buffer_puts(&b,", want down");
}

int main(int argc,char **argv)
{
  int fdsvdir;
  char *dir;

  ++argv;
  if(!*argv) usage();

  if (chdir(svcdir) == -1)
    errint(errno,B("Unable to chdir to service dir: ",svcdir));

  fdsvdir = open_read(".");
  if ((fdsvdir == -1))
    errint(errno,B("unable to open service directory: ",svcdir));

  while ((dir = *argv++)) {
    doit(dir);
    buffer_putsflush(&b,"\n");
    if (fchdir(fdsvdir) == -1)
      errint(errno,B("unable to set service directory: ",svcdir));
  }
  _exit(0);
}
