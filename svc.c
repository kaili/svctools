/********************************************************************************
 * svc (executable file)                                                        *
 *                                                                              *
 *  Author: Kai Peter, ©2022 - present                                          *
 *  Origin: D.J. Bernstein (daemontools-0.76), ©1999-2001, 2007                 *
 * License: ISC                                                                 *
 * Version: 0.79                                                                *
 *                                                                              *
 * Description: Controls services monitored by supervise.                       *
 *******************************************************************************/
#define ME "svc"
#define VERSION "0.79"

#include <unistd.h>
#include "ndelay.h"
#include "errmsg.h"
#include "open.h"
#include "getoptb.h"
#include "buffer.h"
#include "byte.h"
#include "sig.h"

#include "svcdir.h"

int datalen = 0;
char data[20];

buffer b;
char bspace[1];

void version(void) { buffer_putsflush(buffer_1,B(ME,"-",VERSION,"\n")); }

void usage(int e) {
  if (!e) version();
  buffer_puts(buffer_2,"Usage: svc options [service [service] ...]\n");
  buffer_puts(buffer_2,"Options:\n");
  buffer_puts(buffer_2,"\t-u    start a service, restart the service if it dies\n");
  buffer_puts(buffer_2,"\t-d    stop a service, do not restart it\n");
  buffer_puts(buffer_2,"\t-o    start once, do not restart\n");
  buffer_puts(buffer_2,"\t-x    exit\n");
  buffer_puts(buffer_2,"\t-p    pause\n");
  buffer_puts(buffer_2,"\t-c    continue\n");
  buffer_puts(buffer_2,"\t-h    hup\n");
  buffer_puts(buffer_2,"\t-a    alarm\n");
  buffer_puts(buffer_2,"\t-i    interupt\n");
  buffer_puts(buffer_2,"\t-t    term\n");
  buffer_puts(buffer_2,"\t-k    kill\n");
  buffer_puts(buffer_2,"\t-V    print version\n");
  buffer_flush(buffer_2);
  _exit(e);
}

int main(int argc,char **argv)
{
  int opt;
  int fd;
  int fdsvdir;
  char *dir;

  sig_ignore(sig_pipe);

  while ((opt = getopt(argc,argv,"udopchaitkxV")) != opteof) {
    if (opt == 'V') { version(); _exit(0); }
    if (opt == '?') usage(EINVAL);
    else
      if (datalen < sizeof data)
        if (byte_chr(data,datalen,opt) == datalen)
          data[datalen++] = opt;
  }
  argv += optind;

  if (chdir(svcdir) == -1)
    errint(errno,B("Unable to chdir to service directory: ",svcdir));

  fdsvdir = open_read(".");
  if (fdsvdir == -1)
    errint(errno,"unable to open service directory: ");

  while ((dir = (char *)*argv++)) {
    if (chdir(dir) == -1)
      errint(WARN,B("unable to chdir to ",dir,": "));
    else {
      fd = open_write("supervise/control");
      if (fd == -1)
        if (errno == ENXIO)
          errint(WARN,B("unable to control ",dir,": supervise not running"));
        else
          errint(WARN,B("unable to control ",dir,": "));
      else {
        ndelay_off(fd);
        buffer_init(&b,write,fd,bspace,sizeof bspace);
        if (buffer_putflush(&b,data,datalen) == -1)
          errint(WARN,B("error writing commands to ",dir,": "));
        close(fd);
      }
    }
    if (fchdir(fdsvdir) == -1)
      errint(EHARD,"unable to set directory: ");
  }

  _exit(0);
}
