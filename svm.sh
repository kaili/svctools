#********************************************************************************
# Administration of svctools                                                    *
#                                                                               *
#  Author: Kai Peter, ©2022-QYYY                                                *
# Version: 0.2                                                                  *
# License: ISC                                                                  *
#                                                                               *
# Description: A wrapper to control svctools.                                   *
#********************************************************************************
VERSION=`cat $0 | grep -m 1 'Version' | cut -d\   -f3`       # get version number
ME=`basename $0`

SVDIR="QSVCDIR"

showHelp() {
  printf "Usage $ME <action> [ <service> ] [ <option> ]\n\n"
  printf "Options:\n"
  printf " -D, --stop     \t shutdown service\n"
  printf " -S, --start    \t start service\n"
  printf " -o, --once     \t start service once\n"
  printf " -p, --pause    \t pause service\n"
  printf " -c, --continue \t continue service\n"
  printf " -H, --hup      \t send a HUP signal to service\n"
  printf " -a, --alarm    \t send signal alarm to service\n"
  printf " -i, --int      \t interupt\n"
  printf " -t, --term     \t send term signal to service\n"
  printf " -k, --kill     \t kill service\n"
  printf " -x, --exit     \t exit service\n"
  printf " -R, --restart  \t restart service\n"
  printf " -s, --status   \t show status of service\n"
  printf " -m             \t show messages of readproctitle\n"
  printf " -h, --help     \t show this help screen\n"
  printf " -V, --version  \t show version info and exit\n"
}

error() {
  [ "$1" ] && printf "\033[1mError: $1\033[0m\n"
  EC=1; [ "$2" ] && EC="$2"; exit $EC
}
result() {
  C=`tput cols` ; C=$(($C-4))   # calculate cursor position (column)
  printf "\033[${C}G"           # move cursor to column ${C} in actual line
  [ "$EC"  = "0" ] && printf "\033[32m%s\033[0m\n" "OK"
  [ "$EC" != "0" ] && printf "\033[91m%s\033[0m\n" "!!"
}
svcmsg() {
  ps w `pidof readproctitle` | tail -1 | \
    awk '{$1=$2=$3=$4="";print $0}' | sed 's/^ *//g'
  exit $?
}
chkPrivs() {
  [ `id -u` -eq 0 ] && return
  local SUDO="`which sudo`"
  case "$A" in
   svstat) sudo -l | grep -q '/svstat' && A="$SUDO $A" && return;;
     svc*) sudo -l | grep -q '/svc'    && A="$SUDO $A" && return;;
  esac
  error "insufficient privileges!"
}
status() {
  chkPrivs
  [ ! -n "$S" ] && S=`ls $SVDIR`       # w/o a dedicated service use all services
  # print formatted output
  printf "%-12s %-4s %-5s %-25s %s\n" "Service" "STAT" "PID" "Up/DownTime" "Note"
  $A $S | sed 's|(||; s|)||' | \
          awk '/pid/{ printf "%12s %-4s %s %4s %-4s %8s %s ",
                              $1,  $2,  $4,$5, $6,$7, $8; print $9,$10,$11}
               /down,pause/{ printf "%12s %-10s %4s %-4s %8s ",
                                     $1,  $2,   $3, $4,  $5;
                      $1=$2=$3=$4=$5=""; print $6,$7,$8,$9; }'
  exit $?
}
start() {
  chkPrivs
  local SV
  [ -n "$S" ] && [ "$S" = "all" ] && S=`ls $SVDIR`
  for SV in ${S} ; do
    printf "Starting $SV ..."
    $A -u $SV; EC=$?
    result $EC
  done
}
down() {
  chkPrivs
  local SV
  [ -n "$S" ] && [ "$S" = "all" ] && S=`ls $SVDIR`
  for SV in ${S} ; do
    printf "Shutting down $SV ..."
    $A -d $SV; EC=$?
    result $EC
  done
}
restart() { down; sleep 1; start; }
#longOptArg() {
#  OPTARG=`echo $ARG | cut -d= -f2`
#  [ $DBG -gt 2 ] && echo "Real Opt: $RO Arg: $ARG OptArg: $OPTARG"
#  if [ -z "$OPTARG" ] || [ "$OPTARG" = "$ARG" ] ; then
#     error "$RO requires an argument"
#  fi
#}
#shortOptArg() {
#  [ $STRICT -gt 0 ] && \
#    echo "$RO " | grep -q "^\--[a-zA-Z0-9] " && error "invalid option: $RO"
#  OPTARG="$1"; NOARG="$1"
#}

#chkPath() {
#  local D=$1
#  local A=$2
#  echo "$A" | grep -q "^/" && return
#  printf "\033[1m"; log "Error: $D requires an absolue path.\n"; error
#}

#***** begin arguments processing ***********************************************
cmdArgs() {
  declare -r local P=$@                  # a string with all command line options
  declare -r local O=($P)                # an array with all the arguments
  declare -i local idx=0                 # loop index (counter)

  for ARG in $P ; do
#    RO=`echo $ARG | cut -d= -f1`                      # 'R'eal'O'ption on error
    # Error handling: detect if an argument $ARG was corresponding to an option.
    # If an option requires an argument we use the next value in $P and then set
    # NOARG to this value (which is ${O[idx]}). Before increasing $idx we check
    # that the next (now actual) $ARG is the same like $NOARG. If true we don't
    # treat this as an error in the case statement. See '-l' and 'NOARG*' below.
    if [ $idx -gt 0 ] ; then
       [ "$ARG" = "$NOARG" ] && ARG="NOARG: $ARG (${O[idx]})"
    fi
    idx=`expr $idx + 1`                   # now it is save to increment the index
    case "$ARG" in
          NOARG*) NOARG=""; [ $DBG -gt 1 ] && echo "$ARG";;
    # svc signal actions
       -D|--stop) A="svc"; S="${O[idx]}"; down;;     # shutdown
      -S|--start) A="svc"; S="${O[idx]}"; start;;    # start
       -o|--once) A="svc -o"; S="${O[idx]}";;        # start once, do not restart
      -p|--pause) A="svc -p"; S="${O[idx]}";;        #
   -c|--continue) A="svc -c"; S="${O[idx]}";;        # continue
        -H|--hup) A="svc -h"; S="${O[idx]}";;        # send daemon a HUP signal
      -a|--alarm) A="svc -a"; S="${O[idx]}";;            #
        -i|--int) A="svc -i"; S="${O[idx]}";;            #
       -t|--term) A="svc -t"; S="${O[idx]}";;            # send term signal
       -k|--kill) A="svc -k"; S="${O[idx]}";;            # send kill signal
       -x|--exit) A="svc -x"; S="${O[idx]}";;            # stop service
    -R|--restart) A="svc"   ; S="${O[idx]}"; restart;;   # restart
    # other commands
     -s|--status) A="svstat"; S="${O[idx]}"; status;;    # status
              -m) svcmsg;;                               # readproctitle messages
             add) S="${O[idx]}";;                        # 
             del) S="${O[idx]}";;
       -h|--help) showHelp; exit 0;;
    -V|--version) echo "$ME-$VERSION"; exit 0;;
  -[a-zA-Z0-9]?*) # handle multiple combined short options
                  x=`echo $ARG | tr -d '^-' | tr -d '\n' | wc -m`
                  [ $x -lt 2 ] && error "invalid option: $ARG"
                  xARG=`echo $ARG | sed 's|^-||' | grep -o . | sed 's|^|\-|'`
                  cmdArgs $xARG;;
              -*) error "invalid option: $ARG";;
    esac
    [ "$A" ] && [ -n "$A" ] && break
  done
  echo $S | grep -q "^\-" && error "invalid argument to $ARG: $S"
}

#***** begin processing *********************************************************
[ ! "$1" ] && error "$ME: requires an option, use -h to get available options"
cmdArgs $@
chkPrivs; $A $S
exit $?
