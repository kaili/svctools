# Makefile svctools

GNUMAKEFLAGS += --no-print-directory

SHELL  ?= sh
PACKAGE = svctools
VERSION = `head -i VERSION`
DESTDIR = $(PACKAGE)-bin

TARGETS = svscan svboot supervise svc svcmsg svok svstat
TARGETS+= fghack pgrphack readproctitle svclogd svm
TARGETS+= softlimit setuidgid envuidgid envdir setlock

CCFLAGS = -I./qlibs/include
LDFLAGS = -Lqlibs
COMPILE = ./compile $(CCFLAGS)
LOADBIN = ./load
QLIBS = $(LDFLAGS) -lqlibs

PREFIX = `head -1 ac-prefix`
BINDIR = `grep '^BINDIR' ac-layout | cut -d\  -f2`
SVCDIR = `grep '^SVCDIR' ac-layout | cut -d\  -f2`
#MANDIR = `grep '^MANDIR' ac-layout | cut -d\  -f2`
SVPATH = $(PATH)

.PHONY: install $(TARGETS) man

default all: prepare libs $(TARGETS) man
	@echo `date "+%Y%m%d %H%M"` > .build

# recipe 'chkbash' uses the variable 'BASHERR' to be compact in itself
BASHERR = "\033[91mnot found!\033[0m\\n\033[1m'bash' is required"
BASHERR+= "to configure, build and install this package!\033[0m"
chkbash:
	@echo -n "Checking for bash (required) ... "
	@bash -c "echo OK!" 2>config.log || ( echo -e $(BASHERR) ; exit 2 )

distclean: clean
	@rm -rf $(PACKAGE)-bin $(PACKAGE)-bin.tgz
	@rm -f *.log
	@cd qlibs && make clean
	@cd man && make clean

clean:
	@echo -n Cleaning up files ...
	@rm -f *.o *.a $(TARGETS) .build
	@rm -f compile load install systype ac-* mkusrgrp
	@[ -f "PKGNAME.dist" ] && rm -rf `head -1 PKGNAME.dist`.dist
	@rm -rf *.tmp install.log
	@echo " done!"

conf-stat:
	@[ -f compile ] && [ -f $(LOADBIN) ] && [ -f ac-warn.sh ] || ./configure auto

build-stat: conf-stat
	@[ -f install ] || $(MAKE) default

prepare: conf-stat
	@cat ac-warn.sh ac-insthdr.tpl install.sh \
	| sed -e '/QINSTMAN/{r ac-instman.tpl' -e 'd}' \
	| sed s}QPREFIX}"$(PREFIX)"}g \
	| sed s}QBINDIR}"$(BINDIR)"}g \
	| sed s}QMANDIR}"$(ETCDIR)"}g \
	> install
	@chmod 744 install

install: build-stat
	@./install

binpkg:
	$(eval export QUBE_BUILD=bindist)
	@$(MAKE) build-stat
	DESTDIR=$(DESTDIR) $(SHELL) build.in/mk-bindist

libs qlibs: $(eval export QLIBS_LOCAL_INSTALL=yes)
	@cd qlibs ; $(MAKE) all

man:
	@echo Creating man pages ...
	@cd man ; $(MAKE)

objs: ac-files
	$(COMPILE) ac-*.c
	$(COMPILE) deepsleep.c
#	$(COMPILE) match.c
#	$(COMPILE) timestamp.c

envdir:
	$(COMPILE) envdir.c
	$(LOADBIN) envdir $(QLIBS)

envuidgid:
	$(COMPILE) envuidgid.c
	$(LOADBIN) envuidgid $(QLIBS)

fghack:
	$(COMPILE) -Wno-unused-result fghack.c
	$(LOADBIN) fghack $(QLIBS)

fifo.o:
	./compile fifo.c

matchtest: objs
#	$(COMPILE) matchtest.c
#	$(LOADBIN) matchtest match.o $(QLIBS)

#multilog:
#	$(COMPILE) multilog.c
#	$(LOADBIN) multilog deepsleep.o timestamp.o match.o $(QLIBS) -l:time.a

pgrphack:
	$(COMPILE) pgrphack.c
	$(LOADBIN) pgrphack $(QLIBS)

readproctitle:
	$(COMPILE) readproctitle.c
	$(LOADBIN) readproctitle $(QLIBS)

rts: envdir envuidgid fghack matchtest multilog pgrphack \
readproctitle rts.tests setlock setuidgid softlimit supervise svc \
svok svscan svscanboot svstat tai64n tai64nlocal
	env - /bin/sh rts.tests 2>&1 | cat -v > rts

setlock:
	$(COMPILE) setlock.c
	$(LOADBIN) setlock $(QLIBS)

setuidgid:
	$(COMPILE) setuidgid.c
	$(LOADBIN) setuidgid $(QLIBS)

softlimit:
	$(COMPILE) softlimit.c
	$(LOADBIN) softlimit $(QLIBS)

supervise: objs
	$(COMPILE) -Wno-unused-result supervise.c fifo.c
	$(LOADBIN) supervise deepsleep.o $(QLIBS) -l:libqtime.a fifo.o
	chmod 0750 $@

svc:
	$(COMPILE) svc.c
	$(LOADBIN) svc ac-svcdir.o $(QLIBS)
	chmod 0750 $@

svm:
	cat ac-warn.sh svm.sh \
	| sed s}DEVNULL}"`head -1 ac-devnull`"}g \
	| sed s}QBINDIR}$(BINDIR)}g \
	| sed s}QSVCDIR}$(SVCDIR)}g \
	| sed s}QSVPATH}$(SVPATH)}g > $@
	chmod 755 $@

svok:
	$(COMPILE) svok.c
	$(LOADBIN) svok $(QLIBS)

svscan: objs
	$(COMPILE) $@.c
	$(LOADBIN) svscan deepsleep.o ac-svcdir.o $(QLIBS) -l:libqtime.a
	chmod 0750 $@

svboot:
	@rm -f $@
	cat ac-warn.sh svboot.sh \
	| sed s}DEVNULL}"`head -1 ac-devnull`"}g \
	| sed s}QBINDIR}$(BINDIR)}g \
	| sed s}QSVCDIR}$(SVCDIR)}g \
	| sed s}QSVPATH}$(SVPATH)}g > $@
	chmod 555 $@

svcmsg:
	@rm -f $@
	cat ac-warn.sh svcmsg.sh > $@
	chmod 744 $@

svstat: objs
	$(COMPILE) $@.c
	$(LOADBIN) svstat ac-svcdir.o $(QLIBS)
#	-lqtime

#tai64n: objs
#	$(COMPILE) tai64n.c
#	$(LOADBIN) tai64n timestamp.o $(QLIBS) -l:time.a

#tai64nlocal:
#	$(COMPILE) tai64nlocal.c
#	$(LOADBIN) tai64nlocal $(QLIBS)

ac-files:
	@echo "char SHELL[] = \"`head -1 ac-shell`\";" > ac-shell.c
	@echo "char *svcdir = \"$(SVCDIR)\";" > ac-svcdir.c
