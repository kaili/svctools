svctools is a fork of daemontools-0.76. It is part of the init system of qLinux.

Some people may ask: why another fork and not using runit or s6? Now, the reason
is that none of these packages will work with qLinux out of the box. The sources
would have been touched  in any case.  Thus the decision was made  to start with
the original.
