#********************************************************************************
# Define special values for NetBSD systems                                      *
#********************************************************************************

include $(QUBEDIR)/sysdeps/bsd.mk

PWLOCKUSR = usermod -C yes
