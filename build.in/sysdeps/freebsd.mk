#********************************************************************************
# Define special values for FreeBSD systems                                     *
#********************************************************************************

include $(QUBEDIR)/sysdeps/bsd.mk

PWLOCKUSR = pw lock
