#!/config/sh                              # just and only for syntax highlighting
#********************************************************************************
# qware build environment - qube extension (part of configure)                  *
#                                                                               *
#  Author: Kai Peter, ©2022-present                                             *
# Version: 0.1                                                                  *
# License: ISC                                                                  *
#                                                                               *
# Description: This is the place for developers to extend configure to do real  *
#              package specific stuff not covered by qube. It will be executed  *
#              as the last step. It have to be named 'package.inc'.             *
#********************************************************************************

