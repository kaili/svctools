Usage ./configure [ -h | --help ]

    -h|--help    print this help and exit

All configuration is done via files, no command line parameter supported.
