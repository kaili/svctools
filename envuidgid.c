/********************************************************************************
 * envuidgid (executable file)                                                  *
 *                                                                              *
 *  Author: Kai Peter, ©2022 - present                                          *
 *  Origin: D.J. Bernstein (daemontools-0.76), ©1999-2001, 2007                 *
 * License: ISC                                                                 *
 * Version: 0.77                                                                *
 *                                                                              *
 * Description: Runs a program under specific environment vaiables.             *
 *******************************************************************************/
#define ME "envuidgid"
#define VERSION "0.77"

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include "errmsg.h"
#include "strerr.h"
#include "pathexec.h"
#include "qstrings.h"

void nomem(void) { errmem; }

const char *account;
struct passwd *pw;

int main(int argc,const char *const *argv)
{
  char *id;

  account = *++argv;
  if (!account || !*++argv)
    errint(ESOFT,"usage: envuidgid account child");

  pw = getpwnam(account);
  if (!pw)
    errint(EHARD,B("unknown account ",(char *)account));

  id = fmtnum(pw->pw_gid);
  if (!pathexec_env("GID",id)) errmem;
  id = fmtnum(pw->pw_uid);
  if (!pathexec_env("UID",id)) errmem;

  pathexec((char **)argv);
  errint(EHARD,B("unable to run ",(char *)*argv,": "));
}
