/********************************************************************************
 * fghack (executable file)                                                     *
 *                                                                              *
 *  Author: Kai Peter, ©2022 - present                                          *
 *  Origin: D.J. Bernstein (daemontools-0.76), ©1999-2001, 2007                 *
 * License: ISC                                                                 *
 * Version: 0.77                                                                *
 *                                                                              *
 * Description: Run a "backgrounding" daemon in foreground.                     *
 *******************************************************************************/
#define ME "fghack"
#define VERSION "0.77"

#include <unistd.h>
#include "errmsg.h"
#include "pathexec.h"
#include "wait.h"

int pid;

int main(int argc,const char * const *argv,const char * const *envp)
{
  char ch;
  int wstat;
  int pi[2];
  int i;

  if (!argv[1])
    errint(ESOFT,"fghack: usage: fghack child");

  if (pipe(pi) == -1)
     errint(errno,"unable to create pipe: ");

  switch(pid = fork()) {
    case -1:
      errint(errno,"unable to fork: ");
    case 0:
      close(pi[0]);
      for (i = 0;i < 30;++i)
        dup(pi[1]);
      pathexec_run((char *)argv[1],(char **)argv + 1,(char **)envp);
      errint(EHARD,B("unable to run ",(char *)argv[1],": "));
  }

  close(pi[1]);

  for (;;) {
//    i = buffer_unixread(pi[0],&ch,1);
    i = read(pi[0],&ch,1);
    if ((i == -1) && (errno == EINTR)) continue;
    if (i == 1) continue;
    break;
  }

  if (wait_pid(&wstat,pid) == -1)
    errint(errno,"wait failed: ");
  if (wait_crashed(wstat))
    errint(EHARD,"child crashed");
  _exit(wait_exitcode(wstat));
}
