#********************************************************************************
# Print services errors from "readproctitle"                                    *
#                                                                               *
#  Author: Kai Peter, ©2022-present                                             *
# Version: 0.1                                                                  *
# License: ISC                                                                  *
#                                                                               *
# Description: Read the output of readproctitle from ps command.                *
#********************************************************************************
VERSION=`cat $0 | grep -m 1 'Version' | cut -d\   -f3`       # get version number

ps w `pidof readproctitle` | tail -1 | \
  awk '{$1=$2=$3=$4="";print $0}' | sed 's/^ *//g'
