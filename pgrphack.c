/********************************************************************************
 * envdir (executable file)                                                     *
 *                                                                              *
 *  Author: Kai Peter, ©2022 - present                                          *
 *  Origin: D.J. Bernstein (daemontools-0.76), ©1999-2001, 2007                 *
 * License: ISC                                                                 *
 * Version: 0.77                                                                *
 *                                                                              *
 * Description: Run a program in a differant process group.                     *
 *******************************************************************************/
#define ME "pgrphack"
#define VERSION "0.77"

#include <unistd.h>
#include "errmsg.h"
#include "pathexec.h"

int main(int argc,char **argv,char **envp)
{
  if (!argv[1]) errint(ESOFT,"pgrphack: usage: pgrphack child");
  setsid(); /* shouldn't fail; if it does, too bad */
  pathexec_run(argv[1],argv + 1,envp);
  errint(errno,B("pgrphack: fatal: ","unable to run ",argv[1],": "));
}
