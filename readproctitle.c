/********************************************************************************
 * readproctitle (executable file)                                              *
 *                                                                              *
 *  Author: Kai Peter, ©2020 - present                                          *
 *  Origin: D.J. Bernstein (daemontools-0.76), ©1999-2001, 2007                 *
 * License: ISC                                                                 *
 * Version: 0.77.1                                                              *
 *                                                                              *
 * Description: Print service errors through 'procps'.                          *
 *******************************************************************************/
#define ME "readproctitle"
#define VERSION "0.77.1"

#include <unistd.h>
#include "errmsg.h"

int main(int argc,char **argv)
{
  char *buf;
  unsigned int len;
  int i;
  char ch;

  if (argc < 2) _exit(100);
  buf = argv[argc - 1];
  len = 0;
  while (buf[len]) buf[len++] = '.';
  if (len < 5) _exit(100);

  for (;;)
    switch(read(0,&ch,1)) {
      case 1:
    if (ch) {
      for (i = 4;i < len;++i) buf[i - 1] = buf[i];
      buf[len - 1] = ch;
    }
    break;
      case 0:
    _exit(0);
      case -1:
    if (errno != EINTR) _exit(errno);
    }
}
