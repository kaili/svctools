/********************************************************************************
 * envdir (executable file)                                                     *
 *                                                                              *
 *  Author: Kai Peter, ©2022 - present                                          *
 *  Origin: D.J. Bernstein (daemontools-0.76), ©1999-2001, 2007                 *
 * License: ISC                                                                 *
 * Version: 0.77                                                                *
 *                                                                              *
 * Description: Run a program with modifired environment.                       *
 *******************************************************************************/
#define ME "envdir"
#define VERSION "0.77"

#include <unistd.h>
#include "byte.h"
#include "errmsg.h"
#include "direntry.h"
#include "open.h"
#include "pathexec.h"
#include "readclose.h"

void die_usage(void) { errint(EHARD,"envdir: usage: envdir dir child"); }
void nomem(void) { errmem; }

static stralloc sa;

int main(int argc,const char *const *argv)
{
  int fdorigdir;
  char *fn;
  DIR *dir;
  direntry *d;
  int i;

  if (!*argv) die_usage();

  if (!*++argv) die_usage();
  fn = (char *)*argv;

  if (!*++argv) die_usage();

  fdorigdir = open_read(".");
  if (fdorigdir == -1)
    errint(errno,"unable to switch to current directory: ");
  if (chdir(fn) == -1)
    errint(errno,B("unable to switch to directory ",fn,": "));

  dir = opendir(".");
  if (!dir)
    errint(errno,B("unable to read directory ",fn,": "));
  for (;;) {
    errno = 0;
    d = readdir(dir);
    if (!d) {
      if (errno)
        errint(ENOENT,B("unable to read directory ",fn,": "));
      break;
    }
    if (d->d_name[0] != '.') //{
      if (openreadclose(d->d_name,&sa,256) == -1) {
        errint(errno,B("unable to read ",fn,"/",d->d_name,": "));
      if (sa.len) {
        sa.len = byte_chr(sa.s,sa.len,'\n');
        while (sa.len) {
      if (sa.s[sa.len - 1] != ' ')
        if (sa.s[sa.len - 1] != '\t')
          break;
      --sa.len;
        }
        for (i = 0;i < sa.len;++i)
      if (!sa.s[i]) sa.s[i] = '\n';
      if (!stralloc_0(&sa)) nomem();
      if (!pathexec_env(d->d_name,sa.s)) nomem();
      }
      else {
        if (!pathexec_env(d->d_name,0)) nomem();
      }
    }
  }
  closedir(dir);

  if (fchdir(fdorigdir) == -1) {
    errint(errno,B("unable to switch to starting directory: "));
  }
  close(fdorigdir);

  pathexec((char **)argv);
  errint(errno,B("unable to run ",(char *)*argv,": "));
}
