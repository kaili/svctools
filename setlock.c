/********************************************************************************
 * setlock (executable file)                                                    *
 *                                                                              *
 *  Author: Kai Peter, ©2022 - present                                          *
 *  Origin: D.J. Bernstein (daemontools-0.76), ©1999-2001, 2007                 *
 * License: ISC                                                                 *
 * Version: 0.77                                                                *
 *                                                                              *
 * Description: Run a program wich a file locked.                               *
 *******************************************************************************/
#define ME "setlock"
#define VERSION "0.77"

#include <unistd.h>
#include "lock.h"
#include "open.h"
#include "errmsg.h"
#include "pathexec.h"
#include "getoptb.h"

void usage() {
  errint(ESOFT,"setlock: usage: setlock [ -nNxX ] file program [ arg ... ]");
}

int flagndelay = 0;
int flagx = 0;

int main(int argc,char **argv,char **envp)
{
  int opt;
  int fd;
  char *file;

  while ((opt = getopt(argc,argv,"nNxX")) != opteof)
    switch(opt) {
      case 'n': flagndelay = 1; break;
      case 'N': flagndelay = 0; break;
      case 'x': flagx = 1; break;
      case 'X': flagx = 0; break;
      default: usage();
    }

  argv += optind;
  if (!*argv) usage();
  file = *argv++;
  if (!*argv) usage();

  fd = open_append(file);
  if (fd == -1) {
    if (flagx) _exit(0);
    errint(EHARD,B("unable to open ",file,": "));
  }

  if ((flagndelay ? lock_exnb : lock_ex)(fd) == -1) {
    if (flagx) _exit(0);
    errint(EHARD,B("unable to lock ",file,": "));
  }

  pathexec_run(*argv,argv,envp);
  errint(EHARD,B("unable to run ",*argv,": "));
}
