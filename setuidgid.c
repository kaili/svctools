/********************************************************************************
 * setuidgid (executable file)                                                  *
 *                                                                              *
 *  Author: Kai Peter, ©2022 - present                                          *
 *  Origin: D.J. Bernstein (daemontools-0.76), ©1999-2001, 2007                 *
 * License: ISC                                                                 *
 * Version: 0.77                                                                *
 *                                                                              *
 * Description: Run a program under an accounts uid and gid.                    *
 *******************************************************************************/
#define ME "seuidgid"
#define VERSION "0.77"

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include "prot.h"
#include "errmsg.h"
#include "pathexec.h"

char *account;
struct passwd *pw;

int main(int argc,char **argv,char **envp)
{
  account = *++argv;
  if (!account || !*++argv)
    errint(EINVAL,"usage: setuidgid account program");

  pw = getpwnam(account);
  if (!pw)
    errint(EINVAL,B("unknown account ",account));

  if (prot_gid(pw->pw_gid) == -1)
    errint(errno,"unable to setgid: ");
  if (prot_uid(pw->pw_uid) == -1)
    errint(errno,"unable to setuid: ");

  pathexec_run(*argv,argv,envp);
  errint(errno,B("unable to run ",*argv,": "));
}
